# Getting Started

First, clone the repository:

```bash
git clone https://gitlab.com/Abhisheks763/ipl.git
```

then, cd into the project directory

```bash
cd ipl/
```

then, install all the dependencies

```bash
npm i
```

convert csv files in data to json files

```bash
npm run csvtojson
```

finally, run the project

```bash
npm start
```

## Output

The output of the project is saved in a folder in `JSON` format.

Output folder path:

```bash
/src/public/output
```
