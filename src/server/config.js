// @ts-check
/**
 * This module contains absolute path to csv files and output files
 * @module config
 */

const path = require('path')

/**
 * Path to the matches.csv file
 * @type {string}
 */

const pathToMatches = path.join(__dirname,'../','data/matches.csv')

/**
 * Path to the deliveries.csv file
 * @type {string}
 */

const pathToDeliveries = path.join(__dirname,'../','data/deliveries.csv')

/**
 * Path to the output folder
 * @type {string}
 */
const pathToOutput = path.join(__dirname,'../','public/output')

/**
 * Path to the data folder
 * @type {string}
 */
 const pathToData = path.join(__dirname,'../','data/')

module.exports = {    
    pathToMatches,
    pathToDeliveries,
    pathToOutput,
    pathToData
}