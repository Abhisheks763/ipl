//@ts-check
/**
 * @file src/server/index.js is the root file for the project
 * @author Abhishek Singh
 * This module invokes all functions from here
 */



const fs = require('fs')

//absolute data and output folder
const {
    pathToOutput,
    pathToData
} = require('./config.js')

const pathToMatchesJson = pathToData + 'matches.json'
const pathToDeliveriesJson = pathToData + 'deliveries.json'


//all paths to local js files each containing a function
const matchesPlayedPerYear = require('./matchesPlayedPerYear.js')
const matchesWonPerTeamPerYear = require('./matchesWonPerTeam.js')
const extraRunsPerTeam2016 = require('./extraRunsPerTeam2016.js')
const top10EconomicalBowlers2015 = require('./Top10EconomicalBowlers2015.js')

/**
 * @type {object} - data from matches.json
 */
let matchesData;

/**
 * @type {object} - data from deliveries.json
 */
let deliveriesData;



fs.readFile(pathToMatchesJson, (err, data) => {
    if (err) {
        console.log(err)
    } else {
        matchesData = JSON.parse(data.toString())

        //calling function matchesPlayedPerYear and matchesWonPerTeamPerYear
        matchesPlayedPerYear(matchesData, pathToOutput)

        matchesWonPerTeamPerYear(matchesData, pathToOutput)
    }
})

fs.readFile(pathToDeliveriesJson, (err, data) => {
    if (err) {
        console.log(err)
    } else {
        deliveriesData = JSON.parse(data.toString())

        // calling function extraRunsPerTeam2016 and top10EconomicalBowlers2015
        extraRunsPerTeam2016(matchesData, deliveriesData, pathToOutput)

        top10EconomicalBowlers2015(matchesData, deliveriesData, pathToOutput)

    }
})