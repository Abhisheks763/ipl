//@ts-check
/**
 * This module accepts matches and deliveries data to return the object with extra runs conceded per team in year 2016.
 * @module extraRunsPerTeam2016
 */

const fs = require('fs')

/**
 * Takes matches and deliveries data to find extra runs conceded by per team in year 2016
 * @param {Array<object>} matchesData - Matches data as an array of objects , where each object contains data of one match
 * @param {Array<object>} deliveriesData - Deliveries data as an array of objects , where each object contains data of one delivery
 * @param {string} pathToOutput - path to output folder
 * @returns {object} 
 */
function extraRunsPerTeam(matchesData, deliveriesData, pathToOutput) {
    
    /**
     * Extra run conceded by each team
     * Filter matches data for season==2016
     * @type {object} all extra runs in 2016 per team in format {team1:total_extra_run,team2:total_extra_run,......}
     */
    let extraRunsPerTeam = matchesData.filter(obj => obj['season'] == 2016).reduce((accumulator_m, currentMatchObj) => {
        
        /**
         * Extra run conceded in this match(currentMatchObj)
         * Reduce accumulates extra run from each delivery of current match.
         * @type {object} all extra runs in this match in format {team1:extra_run,team2:extra_run}
         */        
        let extraRunByCurrentObj = deliveriesData.reduce((accumulator_d, currentDeliveryObj) => {
            
            if (currentMatchObj['id'] == currentDeliveryObj['match_id']) {
                if (currentDeliveryObj['bowling_team'] in accumulator_d) {
                    accumulator_d[currentDeliveryObj['bowling_team']] += parseInt(currentDeliveryObj['extra_runs'])
                } else {
                    accumulator_d[currentDeliveryObj['bowling_team']] = parseInt(currentDeliveryObj['extra_runs'])
                }
            }
            return accumulator_d

        }, {})
        
        Object.entries(extraRunByCurrentObj).map(arr => {
            if (arr[0] in accumulator_m) {
                accumulator_m[arr[0]] += arr[1]
            } else {
                accumulator_m[arr[0]] = arr[1]
            }
        })
        return accumulator_m

    }, {})
    
    /**
     * extraRunsPerTeam object converted to string
     * @type {string}
     */

    let jsonString = JSON.stringify(extraRunsPerTeam, null, 2)
    fs.writeFile(pathToOutput + '/extraRunsPerTeam2016.json', jsonString,
        err => err ? console.log(err) : console.log('extraRunsPerTeam2016.json saved in output'))

}

module.exports = extraRunsPerTeam;