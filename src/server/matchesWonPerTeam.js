//@ts-check
/**
 * This module accepts matches  to return the object with matches won per team per year
 * @module matchesWonPerTeamPerYear
 */

const fs = require('fs')

/**
 * Takes matches to find number of matches won per by per team per year
 * @param {Array<object>} matchesData - Matches data as an array of objects , where eah object contains data of one match
 * @param {string} pathToOutput - path to output folder
 * @returns {object} 
 */
function matchesWonPerTeamPerYear(matchesData, pathToOutput) {

    /**
     * @type {object} - No of matches won per Team per year in format {team1{year1:no,year2:no,..},....}
     */
    let matchesWonPerTeam = matchesData.reduce((accumulator, currentObj) => {

        if (!(currentObj['winner'] in accumulator)) {
            accumulator[currentObj['winner']] = {}
        }

        if (currentObj['season'] in accumulator[currentObj['winner']]) {
            accumulator[currentObj['winner']][currentObj['season']] += 1
        } else {
            accumulator[currentObj['winner']][currentObj['season']] = 1
        }

        return accumulator

    }, {})

    /**
     * matchesWonPerTeam object converted to string
     * @type {string}
     */
    let jsonString = JSON.stringify(matchesWonPerTeam, null, 2)
    fs.writeFile(pathToOutput + '/matchesWonPerTeamPerYear.json', jsonString, (err) => {
        err ? console.log(err) : console.log('matchesWonPerTeamPerYear.json saved in output')
    })
}

module.exports = matchesWonPerTeamPerYear;