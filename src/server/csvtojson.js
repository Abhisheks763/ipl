/**
 * @module csvtojson
 * This module converts matches.csv and deliveries.csv to json files
 */


csv = require('csvtojson')
fs = require('fs')

const {
    pathToMatches,
    pathToDeliveries,
    pathToData
} = require('./config.js')


csv().fromFile(pathToMatches).then((matchesData) => {
    const matchesDataString = JSON.stringify(matchesData, null, 2);


    fs.writeFile(pathToData + 'matches.json', matchesDataString,(err)=>{
        err?console.log(err):console.log('matches.json saved to data')
    })

})

csv().fromFile(pathToDeliveries).then((deliveryData) => {
    const deliveryDataString = JSON.stringify(deliveryData, null, 2)

    fs.writeFile(pathToData + 'deliveries.json', deliveryDataString, (err) =>{
        err ? console.log(err) : console.log('deliveries.json saved to data')})
})