//@ts-check
/**
 * This module accepts matches  data to return the object with number of matches played per year.
 * @module matchesPlayedPerYear
 */

const fs = require('fs')

/**
 * Takes matches to find number of matches played per year.
 * @param {Array<object>} matchesData - Matches data as an array of objects , where eah object contains data of one match
 * @param {string} pathToOutput - path to output folder
 * @returns {object} 
 */

function matchesPlayedPerYear(matchesData, pathToOutput) {
    /**
     * @type {object} - No of matches played each year in format {year1:no_of_matches1,....}
     */
    let matchesPlayedPerYear = matchesData.reduce((accumulator, currentObj) => {

        if (currentObj['season'] in accumulator) {

            accumulator[currentObj['season']] += 1

        } else {

            accumulator[currentObj['season']] = 1

        }
        return accumulator

    }, {})

    /**
     * matchesPlayedPerYear object converted to string
     * @type {string}
     */
    const jsonString = JSON.stringify(matchesPlayedPerYear, null, 2);

    fs.writeFile(pathToOutput + '/matchesPlayedPerYear.json', jsonString,
        err => err ? console.log(err) : console.log('matchesPlayedPerYear.json saved in output'))
        
}

module.exports = matchesPlayedPerYear;