//@ts-check
/**
 * This module accepts matches and deliveries data to return the object with top 10 economical bowlers of 2015.
 * @module Top10EconomicalBowlers2015
 */

const fs = require('fs')

/**
 * Takes matches and deliveries data to return the object with top 10 economical bowlers of 2015.
 * @param {Array<object>} matchesData - Matches data as an array of objects , where each object contains data of one match
 * @param {Array<object>} deliveriesData - Deliveries data as an array of objects , where each object contains data of one delivery
 * @param {string} pathToOutput - path to output folder
 * @returns {object} 
 */
function economicalBowlers(matchesData, deliveriesData, pathToOutput) {

    /**
     * Economy of all bowlers in 2015 of all matches
     * Filter matches data for season==2015
     * @type {object} economy of top 10 bowlers from 2015 in format {bowler1:economy,bowler2:economy,....}
     */
    let economyOfBowlers = matchesData.filter(obj => obj['season'] == 2015).reduce((accumulator_m, currentMatchObj) => {

        /**
         * No of valid balls thrown and runs given in this match
         * @type {object} data of each bowler in format {bowler1:[runs_given,legal_balls_thrown],bowler2:.....}
         */
        let bowlerData = deliveriesData.reduce((accumulator_d, currentDeliveryObj) => {
            let currentBowler = currentDeliveryObj['bowler']

            if (currentMatchObj['id'] == currentDeliveryObj['match_id']) {
                if (!(currentBowler in accumulator_d)) {
                    accumulator_d[currentBowler] = [0, 0]
                }

                //accumulator_d in format {bowlername:[runs conceded by bowler,legal balls by bowler]}
                accumulator_d[currentBowler][0] += parseInt(currentDeliveryObj['wide_runs']) +
                    parseInt(currentDeliveryObj['noball_runs']) + parseInt(currentDeliveryObj['batsman_runs'])

                if (parseInt(currentDeliveryObj['wide_runs']) == 0 && parseInt(currentDeliveryObj['noball_runs']) === 0) {
                    accumulator_d[currentBowler][1] += 1
                }
            }

            return accumulator_d

        }, {})


        //mapping bowlerData to add runs and balls from each match into accumulator_m
        Object.entries(bowlerData).map(bowler => {

            if (!(bowler[0] in accumulator_m)) {
                accumulator_m[bowler[0]] = [0, 0]
            }

            accumulator_m[bowler[0]][0] += bowler[1][0]
            accumulator_m[bowler[0]][1] += bowler[1][1]

        })

        return accumulator_m
    }, {})

    /**
     * Economy of all bowlers
     * name and economy of bowlers in array
     * @type {Array} 
     */
    let economicalBowlers2015 = []

    /**
     * Economy of all top 10 bowlers
     * name and economy of bowlers in object
     * @type {object} 
     */
    let top10EconomicalBowlers2015 = {}

    Object.keys(economyOfBowlers).map(bowlerName =>

        economicalBowlers2015.push([bowlerName, economyOfBowlers[bowlerName][0] / (economyOfBowlers[bowlerName][1] / 6)]),

    )

    economicalBowlers2015.sort((a, b) => a[1] - b[1]).slice(0, 10).map(
        bowler_eco => top10EconomicalBowlers2015[bowler_eco[0]] = [bowler_eco[1]])


    /**
     * top10EconomicalBowlers2015 object converted to string
     * @type {string}
     */
    const jsonString = JSON.stringify(top10EconomicalBowlers2015, null, 2)
    fs.writeFile(pathToOutput + '/Top10economicalBowlers2015.json', jsonString, (err) => err ? console.log(err) : console.log('Top10economicalBowlers2015.json saved in output'))
}
module.exports = economicalBowlers;